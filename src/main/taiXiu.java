package main;

import java.text.NumberFormat;
import java.util.Locale;
import java.util.Random;
import java.util.Scanner;

public class taiXiu {
	public static void main(String[] args) {
		double taiKhoan = 1000000.99;
		int luaChonChoi;
		Scanner sc = new Scanner(System.in);
		Locale lc = new Locale("vi","VN");
		NumberFormat numf = NumberFormat.getCurrencyInstance(lc);
		do {
			do {
				System.out.println("-------- Mời bạn chọn lựa --------");
				System.out.println("Nhấn 1 để chơi: ");
				System.out.println("Nhấn phím bất kì để thoát");
				luaChonChoi = sc.nextInt();
				
				if (luaChonChoi == 1) {	
					System.out.println("*** Bắt đầu chơi ***");
					System.out.println("Tài khoản của bạn là: " +numf.format(taiKhoan)+ ". Mời bạn chọn số tiền đặt cược");
					double datCuoc = 0;
					do {
						System.out.println("Số tiền cược phải > 0 và không vượt quá số tiền trong tài khoản!");
						datCuoc = sc.nextDouble();
					} while (datCuoc <= 0 || datCuoc > taiKhoan);
					
					//dat
					int datTaiXiu = 0;
					do {
						System.out.println("*****Đặt 1 là tài, đặt 2 là xỉu*****");
						datTaiXiu = sc.nextInt();
					} while (datTaiXiu!=1 && datTaiXiu !=2);
					
					//lac xuc xac
					Random xucXac1 = new Random();
					Random xucXac2 = new Random();
					Random xucXac3 = new Random();
					
					int gt1 = xucXac1.nextInt(6) + 1;
					int gt2 = xucXac1.nextInt(6) + 1;
					int gt3 = xucXac1.nextInt(6) + 1;
					
					//kq
					int kq = gt1 + gt2 + gt3;
					System.out.println("Kết quả là: " +gt1 + "- " +gt2 + " - " + gt3+" :" +kq);
					if (kq == 3 || kq == 18) {
						taiKhoan = taiKhoan - datCuoc;
						System.out.println("Nhà cái ăn hết, bạn đã thua");
						System.out.println("Số tiền còn lại:" +numf.format(taiKhoan));
					}
					else if (kq >= 4 && kq <= 11) {
						System.out.println("Tổng là: " +kq +" => xỉu");
						if(datTaiXiu == 1) {
							taiKhoan = taiKhoan - datCuoc;
							System.out.println("Bạn đã thua cược!");
							System.out.println("Số tiền còn lại:" +numf.format(taiKhoan));
						}
						if(datTaiXiu == 2) {
							taiKhoan = taiKhoan + datCuoc;
							System.out.println("Bạn đã thắng cược!");
							System.out.println("Tài khoản hiện tại:" +numf.format(taiKhoan));
						}	
					}
					else {
						System.out.println("Tổng là: " +kq +" => tài");
						if(datTaiXiu == 1) {
							taiKhoan = taiKhoan + datCuoc;
							System.out.println("Bạn đã thắng cược!");
							System.out.println("Tài khoản của bạn là:" +numf.format(taiKhoan));
						}
						if(datTaiXiu == 2) {
							taiKhoan = taiKhoan - datCuoc;
							System.out.println("Bạn đã thua cược!");
							System.out.println("Số tiền còn lại là:" +numf.format(taiKhoan));
						}	
					}
				}					
			} while (luaChonChoi != 1);
			if(taiKhoan <= 0) {
				System.out.println("Đi ra chỗ khác, không có tiền mà đòi chơi");
			}
		} while(taiKhoan > 0);
	}
}

